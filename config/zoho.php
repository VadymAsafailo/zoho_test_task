<?php


return [
    'authentification_token' => '',
    'access_token' => '',
    'access_token_validity_time' => '',
    'refresh_token' => '',
    'validity_access_token' => '3600',
    'registration_url' => 'https://accounts.zoho.com/oauth/v2/auth?',
    'request_url' => 'https://accounts.zoho.eu/oauth/v2/token',
    'request_records_url' => 'https://www.zohoapis.eu/crm/v4/',
    'request_settings_url'=>'https://www.zohoapis.eu/crm/v4/settings/fields',
    'scope_module_CRM' => 'scope=ZohoCRM.modules.accounts.READ,ZohoCRM.modules.accounts.CREATE,ZohoCRM.modules.deals.READ,ZohoCRM.modules.deals.CREATE,ZohoCRM.modules.accounts.CREATE,ZohoCRM.settings.ALL',
    'scope_program' => 'scope=ZohoCRM.users.ALL',
    'response_type' => '&response_type=code&access_type=offline',
    'client_id' => env('ZOHO_CLIENT_ID'),
    'redirection_Uri' => env('ZOHO_REDIRECTION_URI'),
];
