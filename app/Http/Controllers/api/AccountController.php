<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AccountRequest;
use App\Http\Resources\AccountResource;
use App\Http\Services\HTTPRequestBuildServices;
use App\Http\Services\TokenServices;
use App\Models\Account;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use function MongoDB\BSON\toJSON;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TokenServices $tokenServices, HTTPRequestBuildServices $httpRequest)
    {
        $admin = User::query()->first();
        $access_token = $tokenServices->getValidAccessToken($admin);

        $fields = ['Account_Name', 'Website', 'Phone'];
        $result = $httpRequest->getAllRecords($access_token, 'Accounts', $fields);

        return response(AccountResource::collection($result['data']), 200);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AccountRequest           $request, TokenServices $tokenServices,
                          HTTPRequestBuildServices $httpRequest)
    {

        $data['data'][0] = $request->validated();
        $module_name ='Accounts';
        $admin = User::query()->first();
        $access_token = $tokenServices->getValidAccessToken($admin);

        $result = $httpRequest->createRecord($access_token, $module_name, $data);
        return $result;

    }


    public function destroy(Account $account)
    {
        $account->delete();
        return response('Account was succesfully deleted', 204);
    }
}
