<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Http\Services\TokenServices;

class CredentialsController extends Controller
{
    public function checkAuthToken()
    {
        $admin = User::query()->first();

        if (!$admin->refresh_token) {
            $url = config('zoho.registration_url') . config('zoho.scope_module_CRM')
                . '&client_id=' . $admin->client_id . config('zoho.response_type')
                . '&redirect_uri=' . config('zoho.redirection_Uri');

            return json_encode(['url' => $url]);
        } else return json_encode(['url' => false]);
    }

    public function generateAuthToken(Request $request,TokenServices $tokenServices )
    {
//        Since login was not generated, it was made an assumption, that each user may have it\'s
//        account. Here we check only for first one.

        $admin = User::query()->first();
        $authTokenRequest = $request[0];

         return $tokenServices->generateAuthToken($admin,$authTokenRequest);

    }

    public function generateAccessToken(TokenServices $tokenServices)
    {
        $admin = USER::query()->first();

        return $tokenServices->generateAccessToken($admin);
    }

    public function getValidAccessToken(TokenServices $tokenServices)
    {
        $admin = USER::query()->first();
        $tokenServices->getValidAccessToken($admin);
    }

    public function revokeToken(TokenServices $tokenServices)
    {
        $admin = USER::query()->first();
       return $tokenServices->revokeToken($admin);
    }

}
