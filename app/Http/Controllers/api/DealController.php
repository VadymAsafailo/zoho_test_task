<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DealRequest;
use App\Http\Resources\AccountResource;
use App\Http\Resources\DealResource;
use App\Http\Services\HTTPRequestBuildServices;
use App\Http\Services\TokenServices;
use App\Models\Deal;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class DealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TokenServices $tokenServices, HTTPRequestBuildServices $httpRequest)
    {
        $admin = User::query()->first();
        $access_token = $tokenServices->getValidAccessToken($admin);

        $fields = ['Deal_Name', 'Stage'];
        $result = $httpRequest->getAllRecords($access_token, 'Deals', $fields);

        return response(DealResource::collection($result['data']), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(DealRequest $request,TokenServices $tokenServices,
                          HTTPRequestBuildServices $httpRequest)
    {
        $data['data'][0] = $request->validated();
        $module_name ='Deals';
        $admin = User::query()->first();
        $access_token = $tokenServices->getValidAccessToken($admin);


        $result = $httpRequest->createRecord($access_token, $module_name, $data);

        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Deal $deal)
    {
        $deal->delete();
        return response('Deal record successfully deleted', 204);

    }
}
