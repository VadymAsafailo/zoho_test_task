<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Services\HTTPRequestBuildServices;
use App\Http\Services\TokenServices;
use App\Models\User;
use Illuminate\Http\Request;

class DataListsController extends Controller
{
    public function getStagesList(TokenServices $tokenServices, HTTPRequestBuildServices $httpServices)
    {
        $admin = User::query()->first();
        $access_token = $tokenServices->getValidAccessToken($admin);

        $response = $httpServices->getDataListValues($access_token, 'Deals', 'Stage');

        return json_encode($response, JSON_PRETTY_PRINT);
    }
}
