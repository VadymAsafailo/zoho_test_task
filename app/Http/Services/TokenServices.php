<?php

namespace App\Http\Services;

use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use function PHPUnit\Framework\isEmpty;

class TokenServices
{

    public function generateAuthToken(Model $admin, $authTokenRequest)
    {

        $authentification_token = $admin->authentification_token;
        if ($authTokenRequest === $authentification_token) {
            return;
        } else {
            $admin->update(['authentification_token' => $authTokenRequest]);

            $response = Http::asForm()->post(config('zoho.request_url'), [
                'grant_type' => 'authorization_code',
                'client_id' => $admin->client_id,
                'client_secret' => $admin->client_secret,
                'redirect_uri' => config('zoho.redirection_Uri'),
                'code' => $authTokenRequest,
            ]);

            if (isset($response['error'])) {
                return response($response['error'], 422);
            } else {
//                To avoid situation, when expiry date is calculated wrongly because of server response, added
//                60 seconds difference

                $admin->update([
                    'access_token' => $response['access_token'],
                    'refresh_token' => $response['refresh_token'],
                    'expiry_access_token' => Carbon::now()->addSeconds($response['expires_in'] - 60),
                ]);
            }
        }
    }


    public function generateAccessToken(Model $admin)
    {
        $response = Http::asForm()->post(config('zoho.request_url'), [
            'refresh_token' => $admin->refresh_token,
            'client_id' => $admin->client_id,
            'client_secret' => $admin->client_secret,
            'grant_type' => 'refresh_token',
        ]);

        if (isset($response['error']) && isset($response['error']) === 'invalid_client') {
            $admin->update([
                'authentification_token' => '',
                'access_token' => '',
                'refresh_token' => ''
            ]);
            return redirect('/');
        }


        if ($response['access_token']) {
            $admin->update([
                'access_token' => $response['access_token'],
                'expiry_access_token' => Carbon::now()->addSeconds($response['expires_in'] - 60),
            ]);
            return response('Access token was successfully regenerated from refresh_token', true,);
        } else {
            return response('Error took place', false);
        }
    }


    public function getValidAccessToken(Model $admin)
    {
        if ($admin->access_token) {
            $date1 = Carbon::now();

            $date2 = Carbon::createFromFormat('Y-m-d H:i:s', $admin->expiry_access_token);
            $timeComparison = ($date2->greaterThan($date1));

            if ($date2->greaterThan($date1)) {
                return $admin->access_token;
            } else if ($admin->refresh_token) {


                $generationResult = $this->generateAccessToken($admin);

                if ($generationResult->status) {
                    $admin=User::query()->first();
                    return $admin->access_token;
                } else {
                    $admin->update([
                        'authentification_token' => '',
                        'access_token' => '',
                        'refresh_token' => ''
                    ]);

                    return redirect('/');
                }
            }
        }
    }

    public function revokeToken(Model $admin)
    {
        try {
            $response = Http::post(config('zoho.request_url' . '/revoke'), [
                'token ' => $admin->refresh_token,
            ]);
            return response('Token was successfully revoked', 204);
        } catch (Exception $exception) {
            return response($exception->getMessage(), 422);
        }

    }


}
