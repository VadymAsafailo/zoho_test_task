<?php

namespace App\Http\Services;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class HTTPRequestBuildServices
{
    public function getAllRecords(string $accessToken, string $module, array $fields)
    {
        $fieldsString = implode(',', $fields);

        $response = Http::withHeaders([
            'Authorization' => 'Zoho-oauthtoken ' . $accessToken,
        ])->get(config('zoho.request_records_url') . $module, [
            'fields' => $fieldsString,
        ]);
        return $response;
    }

    public function getDataListValues(string $accessToken, string $module, string $fieldName)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Zoho-oauthtoken ' . $accessToken,
        ])->get(config('zoho.request_settings_url'), [
            'module' => $module,
        ]);

        $fieldsAll = ($response['fields']);


        $collection = Collection::make($fieldsAll);
        $picking_list = $collection->firstWhere('field_label', '==', $fieldName)['pick_list_values'];

        return $picking_list;
    }


    public function createRecord(string $accessToken, string $module, array $data)
    {
        $response = Http::withBody(json_encode($data), 'application/json')
            ->withHeaders([
                'Authorization' => 'Zoho-oauthtoken ' . $accessToken
            ])
            ->post(config('zoho.request_records_url') . $module);

        return $response;
    }
}
