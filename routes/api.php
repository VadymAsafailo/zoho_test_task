<?php

use App\Http\Controllers\api\AccountController;
use App\Http\Controllers\api\CredentialsController;
use App\Http\Controllers\api\DataListsController;
use App\Http\Controllers\api\DealController;
use Illuminate\Http\Request;

//use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::apiResource('/accounts', AccountController::class);
Route::apiResource('/deals', DealController::class);
Route::get('/verification', [CredentialsController::class, 'checkAuthToken']);
Route::post('/Authentification', [CredentialsController::class, 'generateAuthToken']);
Route::get('/dataLists/stages', [DataListsController::class,'getStagesList']);
