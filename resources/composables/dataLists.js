import {ref} from 'vue'
import {useRouter} from "vue-router";
import axios from "axios";

export default function useDataLists() {
    const router = useRouter();
    const errors = ref('');
    const stageLists = ref([]);


    const getStagesLists = async () => {
        axios.get('/api/dataLists/stages')
            .then(response => {
                stageLists.value = response.data;
            })
    }

    return {errors,stageLists, getStagesLists}
}
