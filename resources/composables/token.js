import {ref} from 'vue'
import {useRoute, useRouter} from "vue-router";
import axios from "axios";


export default function useTokens() {
    const url = ref('');
    const token = ref('');
    const router = useRouter();
    const errors = ref('');

    const checkToken = async () => {
        if (useRoute().query.code) {

            errors.value = ''
            const authToken= useRoute().query.code;

            try {
                await axios.post('/api/Authentification', [authToken])
            } catch (e) {
                if (e.response.status === 422) {
                    errors.value = e.response.data.errors
                }
            }
        } else {
            axios.get('/api/verification')
                .then(response => {
                    url.value = response.data.url;
                    console.log(url.value)
                    if (url.value !== false) {
                        console.log(url.value);
                        router.push({redirect: window.location.href = url.value});
                    }
                })
        }

    }

    return {checkToken};
}
