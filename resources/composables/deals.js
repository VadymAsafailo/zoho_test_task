import {ref} from 'vue'
import {useRouter} from "vue-router";
import axios from "axios";

export default function useDeals() {
    const deals = ref([])
    const router = useRouter();
    const errors = ref('');
    const accounts = ref([])
    const notification = ref('');

    const storeDeal = async (data) => {
        errors.value = ''
        notification.value = ''
        try {
            await axios.post('/api/deals', data).then(response => {
                if (response.data.data[0].code == 'SUCCESS') {
                    notification.value = response.data.data[0].message
                    setTimeout(() => {
                        router.push({name: 'deals.index'})
                    }, 3000)
                } else {
                    notification.value = response.data.data[0].message
                }
            })
        } catch (e) {
            if (e.response.status === 422) {
                errors.value = e.response.data.errors
            }
        }
    }
    const getDeals = async () => {
        axios.get('/api/deals')
            .then(response => {
                deals.value = response.data;
            })
    }

    const destroyDeal = async (id) => {
        await axios.delete('/api/deals/' + id)
    }

    return {deals, errors,notification, getDeals, storeDeal, destroyDeal}
}
