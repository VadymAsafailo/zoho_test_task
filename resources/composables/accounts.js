import {ref} from 'vue'
import {useRouter} from "vue-router";
import axios, {formToJSON} from "axios";

export default function useAccounts() {
    const accounts = ref([])
    const router = useRouter();
    const errors = ref('');
    const notification = ref('');


    const getAccounts = async () => {
        axios.get('/api/accounts')
            .then(response => {
                accounts.value = response.data;
            })
    }

    const storeAccount = async (data) => {
        errors.value = ''
        notification.value = ''
        try {
            await axios.post('/api/accounts', data).then(response => {
                if (response.data.data[0].code == 'SUCCESS') {
                    notification.value = response.data.data[0].message
                    setTimeout(() => {
                        router.push({name: 'accounts.index'})
                    }, 3000)
                } else {
                    notification.value = response.data.data[0].message
                }
            })
        } catch (e) {
            if (e.response.status === 422) {
                errors.value = e.response.data.errors
            }
        }
    }

    const destroyAccount = async (id) => {
        await axios.delete('/api/accounts/' + id)
    }


    return {accounts, errors, notification, getAccounts, storeAccount, destroyAccount}
}
