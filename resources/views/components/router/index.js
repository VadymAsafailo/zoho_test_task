import {createRouter, createWebHistory} from 'vue-router'
import AccountsIndex from "../Accounts/Index.vue"
import AccountsCreate from "../Accounts/Create.vue"
import DealsIndex from "../Deals/Index.vue"
import DealsCreate from "../Deals/Create.vue"

const routes = [
    {
        path: '/',
        name: 'accounts.index',
        component: AccountsIndex
    },

    {
        path: '/accounts/create',
        name: 'accounts.create',
        component: AccountsCreate
    },
    {
        path: '/dealsIndex',
        name: 'deals.index',
        component: DealsIndex
    },
    {
        path: '/deals/creates',
        name: 'deals.create',
        component: DealsCreate
    },

]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default createRouter({
    history: createWebHistory(),
    routes
})


