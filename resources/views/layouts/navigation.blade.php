<nav x-data="{ open: false }" class="bg-white border-b border-gray-100">
    <!-- Primary Navigation Menu -->
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between h-16">
            <div class="flex">

                <!-- Navigation Links -->

                <ul class="flex border-b">
                    <li class="-mb-px mr-1">
                        <a class="bg-white inline-block border-l border-t border-r rounded-t py-2 px-4 text-blue-700 font-semibold" href="http://zoho_testing.loc/">
                            <router-link :to="{name: 'accounts.index'}" class="text-sm font-medium">Accounts list</router-link>
                            </a>
                    </li>
                    <li class="mr-1">
                        <a class="bg-white inline-block py-2 px-4 text-blue-500 hover:text-blue-800 font-semibold" href="http://zoho_testing.loc/dealsIndex">
                            <router-link :to="{name: 'deals.index'}" class="text-sm font-medium"> Deals list</router-link>
                            </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>

</nav>
