import './bootstrap';
import router from "../views/components/router";
import {createApp} from 'vue'
import AccountsIndex from "../views/components/Accounts/Index.vue"
import DealsIndex from "../views/components/Deals/Index.vue"

createApp({
    components: {
        AccountsIndex,
        DealsIndex
    }
})
    .use(router)
    .mount('#app')
