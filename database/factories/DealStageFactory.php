<?php

namespace Database\Factories;

use App\Models\DealStage;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class DealStageFactory extends Factory
{
    private array $dealStages;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $this->dealStages = ['Negotiation', 'Signed contract', 'Agreed documentation', 'Under performance', 'Goods shipped', 'Completed'];
        foreach ($this->dealStages as $dealStage) {
                DealStage::create([
                    'name'=>$dealStage
                ]);
        }
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
